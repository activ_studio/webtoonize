extends TextureButton

#var globals = preload("res://scripts/globals.gd")

onready var globals = get_node("/root/globals")

var MouseOver = false


# Called when the node enters the scene tree for the first time.
func _ready():
	pass
	

#func _input(event):
	#pass
	#if event is InputEventMouseButton:
		#if MouseOver == true:
	#		print("Clicked On Object")
			
	#		root_node.position = Vector2(root_node.position.x-1024,0)

func _on_TextureButton_pressed():
	print(globals.next)
	if globals.next == 0:
		# we go to next diapo
		globals.fade = 1
		if globals.frame_read < globals.frame_qty:
			globals.frame_read += 1
			globals.change_diapo()
		if globals.frame_read == globals.frame_qty :
			if globals.next < len(globals.dialogs):
				#globals.next += 1
				# on cache les boutons next
				self.disabled = true
				self.visible = false
	else :
		# we browse through dialog nodes
		for c in globals.dialogs:
			if c.name == "dialog"+str(globals.next):
				c.visible = true
				self.disabled = false
		if globals.next < len(globals.dialogs):
			globals.next += 1
		else:
			globals.next = 0
