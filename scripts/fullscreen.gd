extends TextureButton


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	refresh_button_apperance()

func refresh_button_apperance():
	if OS.window_fullscreen:
		self.get_stylebox("hover").set_texture(load("res://assets/ui/bouton_affichage_reduire_surbrillance.png")) 
		#$fullscreen.get_stylebox("pressed").set_texture(load("res://assets/ui/bouton_affichage_reduire_surbrillance.png")) 
		self.get_stylebox("normal").set_texture(load("res://assets/ui/bouton_affichage_reduire.png")) 
	else:
		self.get_stylebox("hover").set_texture(load("res://assets/ui/bouton_affichage_agrandir_surbrillance.png")) 
		#$fullscreen.get_stylebox("pressed").set_texture(load("res://assets/ui/bouton_affichage_agrandir_surbrillance.png")) 
		self.get_stylebox("normal").set_texture(load("res://assets/ui/bouton_affichage_agrandir.png")) 


func _on_TextureButton_pressed():
	OS.window_fullscreen = !OS.window_fullscreen 
	globals.play_sound("interact_mouse_click",true)
	refresh_button_apperance()
