extends Node


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var root_node 
var ui
var frame_read = 1
var frame_qty  = 0
export var width = 1024
export var height = 640
var fade = 0
export var fade_speed = 0.01
# définit si on passe à la diapo suivante  0
# le chiffre indique le numero du dialogue
var next = 0
var current_diapo
var dialogs = []


# Called when the node enters the scene tree for the first time.
func _ready():
	self.root_node = get_tree().get_root().get_node("Node2D").get_node("screens")
	self.ui = get_tree().get_root().get_node("Node2D").get_node("ui")
	self.frame_qty = len(root_node.get_children())

func change_diapo():
	globals.current_diapo = "screenframe"+str(globals.frame_read)
	var next = 0
	globals.dialogs = []
	for c in get_node("/root/Node2D/screens/"+globals.current_diapo).get_children():
		if "dialog" in c.name:
			c.visible = false
			globals.next = 1
			globals.dialogs.append(c)

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func play_sound(sound_info,randomized):
	randomize()
	self.ui.get_node(sound_info).play()
	if randomized:
		self.ui.get_node(sound_info).pitch_scale = self.ui.get_node(sound_info).pitch_scale + rand_range(-0.1,0.1)
