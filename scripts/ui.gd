extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
onready var globals = get_node("/root/globals")

var info = {}

func _ready():
	pass


func _on_TextureButton_pressed():
	globals.play_sound("interact_mouse_click",true)


func _on_menuButton_pressed():
	$menu.visible = ! $menu.visible
	globals.play_sound("interact_mouse_click",true)
