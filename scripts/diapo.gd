extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

onready var globals = get_node("/root/globals")

var char_tex = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	globals.current_diapo = "screenframe1"
	globals.change_diapo()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var color = get_parent().get_modulate()
	if globals.fade == 0:
		# la diapo reste
		pass
	elif globals.fade == 2:
		# la diapo reapparait
		var r = color.r+globals.fade_speed
		color = Color(r,r,r,1)
		if r > 1:
			globals.fade = 0
		else :
			get_parent().set_modulate(color)
	elif globals.fade == 1 :
		# la diapo disparait
		var r = color.r-globals.fade_speed
		color = Color(r,r,r,1)
		if r < 0:
			globals.fade = 2
			if globals.frame_read <= globals.frame_qty:
				globals.root_node.position = Vector2(globals.root_node.position.x-globals.width,0)
		else :
			get_parent().set_modulate(color)
	
	
