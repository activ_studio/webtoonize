extends TextureButton


#var globals = preload("res://scripts/globals.gd")
onready var globals = get_node("/root/globals")

# Called when the node enters the sgcene tree for the first time.
func _ready():
	pass 

func _on_TextureButton_pressed():
	globals.diapo_read = 0
	globals.ui.get_node("nextButton").visible = true
	globals.root_node.position = Vector2(0,0)
	globals.play_sound("interact_mouse_click",true)
