extends Sprite


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

enum AT {none, zoom, translateX, translateY}
export(AT) var animation_type

export var anim_to =  0.0
export var anim_speed = 0.0

var to = 0.0

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func zoom():
	if self.to < anim_to:
			var a = 1+(float(self.anim_speed)/10000)
			self.scale *= Vector2(a, a)
			self.to = self.to+float(self.anim_speed)/10000
			
func translateX():
	if self.to < (anim_to+globals.width/2):
			var a = 1+(float(self.anim_speed)/10000)
			if anim_to > 0:
				self.position.x += a
			else:
				self.position.x -= a
	
func translateY():
	if anim_to > 0:  # animation will go down
		print(self.to)
		print(anim_to)
		print(self.position.y)
		if self.to < anim_to:
			var a = 1+(float(self.anim_speed)/10000)
			self.position.y += a
			self.to += a
	else :  # animation will go up
		if self.to > 0:
			var a = 1+(float(self.anim_speed)/10000)
			self.position.y -= a
			self.to -= a

func get_resized_texture(t: Texture, width: int = 0, height: int = 0):
	var image = t.get_data()
	if width > 0 && height > 0:
		image.resize(width, height)
	var itex = ImageTexture.new()
	itex.create_from_image(image)
	return itex

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if globals.current_diapo == self.get_parent().name and globals.fade == 0:
		# globals.fade == 0 : we wait that for all fades to be stopped
		if self.animation_type == 0:
			pass
		elif self.animation_type == 1:
			zoom()
		elif self.animation_type == 2:
			self.to = self.position.x-(globals.width/2)
			translateX()
		elif self.animation_type == 3:
			self.to = self.to #self.position.y-(globals.height/2)
			translateY()
