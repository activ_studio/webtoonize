# Webtoonize

Webtoonize is a godot template including some scripts to help cartoonist make standalone or web interactive cartoon

Official website : https://webtoonize.com

# Installation


1. Create a godot project
2. Clone or download repository and put it in your godot project base directory

# How to use ?

- Leave Globals, UI, screens nodes as they are
- each "screen" display is created by a screenframe node. Just duplicate one if you find it easier
- each screenframe node must follow this naming convention : beginning with the "screenframe" text in their names 
- each screenframe can contain as many element (texts, drawing as needed)
- if you want to have element that appear when the reader click or touch, just use the naming convention : "dialog" following by a number
- if you want to animate some elements, attach the background.gd script to it and change the anim_to and anim_speed value in the Inspector
